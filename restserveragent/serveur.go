package restserveragent

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"sync"
	"time"

	ballot "gitlab.utc.fr/shensiyi/ia04-tp/agt/ballotagent"
	votant "gitlab.utc.fr/shensiyi/ia04-tp/agt/voteragent"
	comsoc "gitlab.utc.fr/shensiyi/ia04-tp/comsoc"
	rad "gitlab.utc.fr/shensiyi/ia04-tp/type"
)

type RestServerAgent struct {
	sync.Mutex
	id   string
	addr string
}

var bltmap = make(ballot.BallotMap)

var vtmap = make(votant.Votermap)

var profilmap = make(map[string]comsoc.Profile)

var altmap = make(map[string][]comsoc.Alternative)

var optionsmap = make(map[string]int)

func NewRestServerAgent(addr string) *RestServerAgent {
	return &RestServerAgent{id: addr, addr: addr}
}

func (rsa *RestServerAgent) checkMethod(method string, w http.ResponseWriter, r *http.Request) bool {
	if r.Method != method {
		w.WriteHeader(http.StatusMethodNotAllowed)
		fmt.Fprintf(w, "method %q not allowed", r.Method)
		return false
	}
	return true
}

// Désérialisation, analyse des données JASON en types de données Go
func (*RestServerAgent) decodeRequestBallot(r *http.Request) (req rad.Request_ballot, err error) {
	buf := new(bytes.Buffer) //Tampon de données d'octet
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req) //Désérialisation
	return
}

func (*RestServerAgent) decodeRequestVote(r *http.Request) (req rad.Request_vote, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}

func (*RestServerAgent) decodeRequestResult(r *http.Request) (req rad.Request_result, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}

func (rsa *RestServerAgent) forNewBallot(w http.ResponseWriter, r *http.Request) {
	if !rsa.checkMethod("POST", w, r) {
		return
	}

	req, err := rsa.decodeRequestBallot(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("bad request"))
		fmt.Fprint(w, err.Error())
		return
	}
	var resp rad.Response_ballot

	switch req.Rule {
	case "majarity":
		blt := ballot.NewBallotagent(req.Rule, req.Deadline, req.Voter_ids, req.Alts, req.Tie_break)
		resp.Ballot_id = blt.ID
		bltmap[blt.ID] = *blt
		alt_number := blt.Alts
		var alt []comsoc.Alternative
		for i := 1; i <= alt_number; i++ {
			alt = append(alt, comsoc.Alternative(i))
		}
		altmap[blt.ID] = alt
	case "borda":
		blt := ballot.NewBallotagent(req.Rule, req.Deadline, req.Voter_ids, req.Alts, req.Tie_break)
		resp.Ballot_id = blt.ID
		bltmap[blt.ID] = *blt
		alt_number := blt.Alts
		var alt []comsoc.Alternative
		for i := 1; i <= alt_number; i++ {
			alt = append(alt, comsoc.Alternative(i))
		}
		altmap[blt.ID] = alt
	case "approval":
		blt := ballot.NewBallotagent(req.Rule, req.Deadline, req.Voter_ids, req.Alts, req.Tie_break)
		resp.Ballot_id = blt.ID
		bltmap[blt.ID] = *blt
		alt_number := blt.Alts
		var alt []comsoc.Alternative
		for i := 1; i <= alt_number; i++ {
			alt = append(alt, comsoc.Alternative(i))
		}
		altmap[blt.ID] = alt
	case "stv":
		blt := ballot.NewBallotagent(req.Rule, req.Deadline, req.Voter_ids, req.Alts, req.Tie_break)
		resp.Ballot_id = blt.ID
		bltmap[blt.ID] = *blt
		alt_number := blt.Alts
		var alt []comsoc.Alternative
		for i := 1; i <= alt_number; i++ {
			alt = append(alt, comsoc.Alternative(i))
		}
		altmap[blt.ID] = alt
	case "copeland":
		blt := ballot.NewBallotagent(req.Rule, req.Deadline, req.Voter_ids, req.Alts, req.Tie_break)
		resp.Ballot_id = blt.ID
		bltmap[blt.ID] = *blt
		alt_number := blt.Alts
		var alt []comsoc.Alternative
		for i := 1; i <= alt_number; i++ {
			alt = append(alt, comsoc.Alternative(i))
		}
		altmap[blt.ID] = alt
	default:
		w.WriteHeader(http.StatusNotImplemented)
		msg := fmt.Sprintf("Unkonwn command '%s'", req.Rule)
		w.Write([]byte(msg))
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("vote créé"))
	serial, _ := json.Marshal(resp)
	w.Write(serial)

}

func (rsa *RestServerAgent) forNewVote(w http.ResponseWriter, r *http.Request) {
	if !rsa.checkMethod("POST", w, r) {
		return
	}

	req, err := rsa.decodeRequestVote(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("bad request"))
		fmt.Fprint(w, err.Error())
		return
	}

	blt, errs := bltmap.Find_Ballot(req.Ballot_id)
	if errs != nil {
		w.WriteHeader(http.StatusNotImplemented)
		w.Write([]byte("the ballot not exist"))
		return
	}

	vt := blt.Find_Votant_in_Ballot(req.Agent_id)
	if !vt {
		w.WriteHeader(http.StatusNotImplemented)
		w.Write([]byte("the votant not exist in the ballot"))
		return
	}

	//Vérifiez si le délai est dépassé
	datestr := blt.Deadline
	t, er := time.Parse(time.UnixDate, datestr)
	if er != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("bad request"))
		fmt.Fprint(w, er.Error())
		return
	}
	//Si la date limite n’est pas encore atteinte
	currentTime := time.Now()
	if t.After(currentTime) {
		_, found := vtmap[req.Agent_id]
		//Si le vote est terminé, c'est-à-dire que le vote a été créé et existe dans vtmap, 403 sera renvoyé.
		if found {
			w.WriteHeader(http.StatusForbidden)
			w.Write([]byte("vote déjà effectué"))
			return
		}
		//S'il n'est pas créé, créez un nouveau type d'électeur
		vot := votant.NewAgent(req.Agent_id, req.Prefs)
		vtmap[req.Agent_id] = *vot
		profilmap[req.Ballot_id] = append(profilmap[req.Ballot_id], req.Prefs)
		if req.Options != nil {
			optionsmap[req.Agent_id] = req.Options[0]
		}
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("vote pris en compte"))
	} else {
		//Si le temps est écoulé, retournez 503
		w.WriteHeader(http.StatusServiceUnavailable)
		w.Write([]byte("la deadline est dépassée"))
		return
	}
}

func (rsa *RestServerAgent) forResult(w http.ResponseWriter, r *http.Request) {
	if !rsa.checkMethod("POST", w, r) {
		return
	}
	req, err := rsa.decodeRequestResult(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("bad request"))
		fmt.Fprint(w, err.Error())
		return
	}
	blt, found := bltmap[req.Ballot_id]
	//Si le ballon n'est pas trouvé, retournez 404
	if !found {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("Not Found"))
		return
	}
	currentTime := time.Now()
	datestr := blt.Deadline
	t, er := time.Parse(time.UnixDate, datestr)
	if er != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("bad request"))
		fmt.Fprint(w, er.Error())
		return
	}
	//Si le temps n'est pas encore écoulé, retournez 425
	if t.After(currentTime) {
		w.WriteHeader(425)
		fmt.Fprintln(w, "Too early")
		return
	}

	var resp rad.Response_result
	switch blt.Rule {
	case "majarity":
		prof := profilmap[req.Ballot_id]
		alts := altmap[req.Ballot_id]
		tiebreak := comsoc.TieBreakFactory(blt.Tie_break)
		swf := comsoc.SWFFactory(comsoc.MajoritySWF, tiebreak)
		rank, errank := swf(prof, alts)
		if errank != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("bad request"))
			fmt.Fprint(w, err.Error())
			return
		}
		scf := comsoc.SCFFactory(comsoc.MajoritySCF, tiebreak)
		resp.Ranking = rank
		winner, errwinner := scf(prof, alts)
		if errwinner != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("bad request"))
			fmt.Fprint(w, err.Error())
			return
		}
		resp.Winner = int(winner)
	case "stv":
		prof := profilmap[req.Ballot_id]
		alts := altmap[req.Ballot_id]
		tiebreak := comsoc.TieBreakFactory(blt.Tie_break)
		swf := comsoc.SWFFactory(comsoc.STV_SWF, tiebreak)
		rank, errank := swf(prof, alts)
		if errank != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("bad request"))
			fmt.Fprint(w, err.Error())
			return
		}
		scf := comsoc.SCFFactory(comsoc.STV_SCF, tiebreak)
		resp.Ranking = rank
		winner, errwinner := scf(prof, alts)
		if errwinner != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("bad request"))
			fmt.Fprint(w, err.Error())
			return
		}
		resp.Winner = int(winner)
	case "copeland":
		prof := profilmap[req.Ballot_id]
		alts := altmap[req.Ballot_id]
		tiebreak := comsoc.TieBreakFactory(blt.Tie_break)
		swf := comsoc.SWFFactory(comsoc.CopelandSWF, tiebreak)
		rank, errank := swf(prof, alts)
		if errank != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("bad request"))
			fmt.Fprint(w, err.Error())
			return
		}
		scf := comsoc.SCFFactory(comsoc.CopelandSCF, tiebreak)
		resp.Ranking = rank
		winner, errwinner := scf(prof, alts)
		if errwinner != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("bad request"))
			fmt.Fprint(w, err.Error())
			return
		}
		resp.Winner = int(winner)
	case "approval":
		prof := profilmap[req.Ballot_id]
		alts := altmap[req.Ballot_id]
		tiebreak := comsoc.TieBreakFactory(blt.Tie_break)
		bl := bltmap[req.Ballot_id]
		vots_id := bl.Voter_ids
		var threshold []int
		for _, j := range vots_id {
			threshold = append(threshold, optionsmap[j])
		}
		swf := comsoc.SWFFactoryapp(comsoc.ApprovalSWF, threshold, tiebreak)
		rank, errank := swf(prof, alts)
		if errank != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("bad request"))
			fmt.Fprint(w, err.Error())
			return
		}
		scf := comsoc.SCFFactory(comsoc.BordaSCF, tiebreak)
		resp.Ranking = rank
		winner, errwinner := scf(prof, alts)
		if errwinner != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("bad request"))
			fmt.Fprint(w, err.Error())
			return
		}
		resp.Winner = int(winner)
	case "borda":
		prof := profilmap[req.Ballot_id]
		alts := altmap[req.Ballot_id]
		tiebreak := comsoc.TieBreakFactory(blt.Tie_break)
		swf := comsoc.SWFFactory(comsoc.BordaSWF, tiebreak)
		rank, errank := swf(prof, alts)
		if errank != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("bad request"))
			fmt.Fprint(w, err.Error())
			return
		}
		scf := comsoc.SCFFactory(comsoc.BordaSCF, tiebreak)
		resp.Ranking = rank
		winner, errwinner := scf(prof, alts)
		if errwinner != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("bad request"))
			fmt.Fprint(w, err.Error())
			return
		}
		resp.Winner = int(winner)
	default:
		w.WriteHeader(http.StatusNotImplemented)
		msg := fmt.Sprintf("Unkonwn rule '%s'", blt.Rule)
		w.Write([]byte(msg))
		return

	}
	w.WriteHeader(http.StatusOK)
	serial, _ := json.Marshal(resp)
	w.Write(serial)
}

func (rsa *RestServerAgent) Start() {
	mux := http.NewServeMux() //Utilisé pour créer un multiplexeur de requêtes HTTP, souvent appelé "ServeMux", utilisé pour acheminer les requêtes HTTP entrantes vers différents gestionnaires (gestionnaires), les gestionnaires spécifiques dépendent du chemin de la requête (URL).
	mux.HandleFunc("/new_ballot", rsa.forNewBallot)
	mux.HandleFunc("/vote", rsa.forNewVote)
	mux.HandleFunc("/result", rsa.forResult)

	// création du serveur http
	s := &http.Server{
		Addr:           rsa.addr,
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20}

	// lancement du serveur
	log.Println("Listening on", rsa.addr)
	go log.Fatal(s.ListenAndServe()) //Il est utilisé pour enregistrer un message d'erreur puis terminer l'exécution du programme. Il est généralement utilisé pour terminer le programme immédiatement lorsqu'une erreur grave ou un problème irrécupérable est rencontré.

}
