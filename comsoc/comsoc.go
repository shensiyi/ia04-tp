package comsoc

import (
	"errors"
	"sort"
)

type Alternative int
type Profile [][]Alternative
type Count map[Alternative]int

type error interface {
	Error() string
}

func rank(alt Alternative, prefs []Alternative) int {
	for i := 0; i < len(prefs); i++ {
		if alt == prefs[i] {
			return i
		}
	}
	return -1
}

func isPref(alt1, alt2 Alternative, prefs []Alternative) bool {
	prefs_alt1 := rank(alt1, prefs)
	prefs_alt2 := rank(alt2, prefs)
	if prefs_alt1 > prefs_alt2 {
		return false
	} else {
		return true
	}
}

func maxCount(count Count) (bestAlts []Alternative) {
	point_map := make(map[int][]Alternative)
	keys := make([]int, 0)
	for i, m := range count {
		point_map[m] = append(point_map[m], i)
	}
	for key := range point_map {
		keys = append(keys, key)
	}
	sort.Ints(keys)
	point_max := keys[len(keys)-1]
	return point_map[point_max]
}

func minCount(count Count) (bestAlts []Alternative) {
	point_map := make(map[int][]Alternative)
	keys := make([]int, 0)
	for i, m := range count {
		point_map[m] = append(point_map[m], i)
	}
	for key := range point_map {
		keys = append(keys, key)
	}
	sort.Ints(keys)
	point_min := keys[0]
	return point_map[point_min]
}

// func checkProfile(prefs Profile) error {
// 	len_max := len(prefs[0])
// 	err1,err2 := false,false
// 	for _,m := range prefs{
// 		if alt_une_fois(m){
// 			if len(m)>len_max{
// 				err2=true
// 				len_max=len(m)
// 			}
// 		}else {
// 			err2=true
// 		}
// 	}
// 	if err1 && err2{
// 		return errors.New("pas complet et alternative apparait deux fois")
// 	}else if err1 && !err2{
// 		return errors.New("alternative apparait deux fois")
// 	}else if !err1 && err2{
// 		return errors.New("pas complet")
// 	}
// 	return nil
// }

func checkProfile(prefs []Alternative, alts []Alternative) error {
	if len(prefs) < len(alts) {
		return errors.New("pas complet")
	} else if len(prefs) > len(alts) {
		return errors.New("apparait deux fois")
	}
	return nil
}

func checkProfileAlternative(prefs Profile, alts []Alternative) error {
	for i := range prefs {
		err := checkProfile(prefs[i], alts)
		if err != nil {
			return errors.New("pas bon")
		}
	}
	return nil
}

func MajoritySWF(p Profile, alts []Alternative) (count Count, err error) {
	err = checkProfileAlternative(p, alts)
	c := make(map[Alternative]int, 0)
	if err != nil {
		return c, err
	}
	for j := 0; j < len(alts); j++ {
		c[alts[j]] = 0

	}
	for i := 0; i < len(p); i++ {
		c[p[i][0]] = c[p[i][0]] + 1
	}
	return c, err
}

func MajoritySCF(p Profile, alts []Alternative) (bestAlts []Alternative, err error) {
	c, e := MajoritySWF(p, alts)
	if e != nil {
		return bestAlts, e
	}
	bestAlts = maxCount(c)
	return bestAlts, e
}

func BordaSWF(p Profile, alts []Alternative) (count Count, err error) {

	// Créer une carte des décomptes
	count = make(map[Alternative]int, 0)

	// Parcourez chaque configuration de préférences
	for _, pref := range p {
		// Parcourez chaque alternative
		for i, alt := range pref {
			// Calculez le score Borda, plus le classement est bas, plus le score est élevé
			count[alt] += len(pref) - (i + 1)
		}
	}

	return count, nil
}

func BordaSCF(p Profile, alts []Alternative) (bestAlts []Alternative, err error) {

	// Obtenez le score Borda de chaque alternative
	count, err := BordaSWF(p, alts)
	if err != nil {
		return nil, err
	}

	// Utilisez la fonction maxCount pour trouver l'alternative avec le score Borda le plus élevé
	return maxCount(count), nil
}

// ApprovalSWF renvoie le nombre de votes positifs pour chaque alternative
func ApprovalSWF(p Profile, alts []Alternative, thresholds []int) (count Count, err error) {

	// Vérifiez la validité du seuil, c'est-à-dire que chaque électeur doit avoir un seuil, sinon aucun n'est renvoyé.
	if len(thresholds) != len(p) {
		return nil, err
	}

	// Créer une carte des décomptes
	count = make(map[Alternative]int, 0)

	// Parcourez chaque configuration de préférences
	for i, pref := range p {
		threshold := thresholds[i]

		// Parcourez chaque alternative
		for _, alt := range pref {
			// Parce que le parcours des préférences commence à partir du premier (0), alors le premier est le candidat le plus préféré. S'il ne dépasse pas le seuil, cela signifie que vous pouvez voter pour elle, et ensuite c'est au tour du deuxième candidat préféré. .analogie
			if threshold > 0 {
				count[alt]++
				threshold--
			}
		}
	}

	return count, nil
}

func ApprovalSCF(p Profile, alts []Alternative, thresholds []int) (bestAlts []Alternative, err error) {
	count, err := ApprovalSWF(p, alts, thresholds)
	if err != nil {
		return nil, err
	}

	// Utilisez la fonction maxCount pour trouver l'alternative avec le plus grand nombre de votes
	return maxCount(count), nil
}

func TieBreak_SCF(alts []Alternative) (Alternative, error) {
	if len(alts) == 0 {
		return -1, errors.New("slice of Alternative is vide")
	}
	al := alts[0]
	return al, nil
}

// func TieBreak_SWF(alts []Alternative) ([]Alternative, error){
// 	al := make([]Alternative,0)
// 	value := make([]int,0)
// 	if len(alts) == 0{
// 		return al,errors.New("slice of Alternative is vide")
// 	}
// 	for i,m := range alts{
// 		value[i]=int(m)
// 	}
// 	sort.Ints(value)
// 	for _,m := range value{
// 		al = append(al, Alternative(m))
// 	}
// 	return al,nil
// }

func TieBreakFactory(orderedAlts []Alternative) func([]Alternative) (Alternative, error) {
	return func(alt []Alternative) (Alternative, error) {
		if len(alt) == 0 {
			return -1, errors.New("slice of Alternative is vide")
		}
		note := make(map[Alternative]int)
		for i := 0; i < len(alt); i++ {
			note[alt[i]] = rank(alt[i], orderedAlts)
		}
		best := minCount(note)
		return best[0], nil
	}
}

func SWFFactoryapp(swf func(p Profile, alts []Alternative, threshold []int) (Count, error), threshold []int, tiebreak func([]Alternative) (Alternative, error)) func(Profile, []Alternative) ([]Alternative, error) {
	return func(p Profile, alts []Alternative) ([]Alternative, error) {
		al := make([]Alternative, 0)
		point_map := make(map[int][]Alternative)
		keys := make([]int, 0)
		//Obtenir Count à l'aide de SWF
		c, e := ApprovalSWF(p, alts, threshold)
		//S'il y a une erreur, revenez directement
		if e != nil {
			return al, e
		}

		//Créer une carte d'Alternatives (valeur) avec le même score (clé)
		for i, m := range c {
			point_map[m] = append(point_map[m], i)
		}
		for key := range point_map {
			keys = append(keys, key)
		}
		sort.Ints(keys)
		//Ordre les alternatives donné en fonction du score
		for i := len(keys) - 1; i >= 0; i-- {
			if len(point_map[keys[i]]) == 1 {
				al = append(al, point_map[keys[i]][0])
			} else if len(point_map[keys[i]]) > 1 {
				len := len(point_map[keys[i]])
				for j := 0; j < len; j++ {
					//À chaque fois, l'alternatif optimal est sélectionné par tie-break.
					alt_tie, e := tiebreak(point_map[keys[i]])
					if e != nil {
						return al, e
					}
					al = append(al, alt_tie)
					//Supprimez les sélectionnés de la tranche Alternative à trier
					indice := rank(alt_tie, point_map[keys[i]])
					point_map[i] = append(point_map[keys[i]][:indice], point_map[keys[i]][indice+1:]...)
				}
			}
		}
		return al, nil

	}
}

func SCFFactoryapp(scf func(p Profile, alts []Alternative, threshold []int) ([]Alternative, error), threshold []int, tiebreak func([]Alternative) (Alternative, error)) func(Profile, []Alternative) (Alternative, error) {
	return func(p Profile, alts []Alternative) (Alternative, error) {
		best, e := ApprovalSCF(p, alts, threshold)
		if e != nil {
			return -1, e
		}
		best_tie, er := tiebreak(best)
		if er != nil {
			return -1, er
		}
		return best_tie, nil
	}
}

func SWFFactory(swf func(p Profile, alts []Alternative) (Count, error), tiebreak func([]Alternative) (Alternative, error)) func(Profile, []Alternative) ([]Alternative, error) {
	return func(p Profile, alts []Alternative) ([]Alternative, error) {
		al := make([]Alternative, 0)
		point_map := make(map[int][]Alternative)
		keys := make([]int, 0)
		//Obtenir Count à l'aide de SWF
		c, e := swf(p, alts)
		//S'il y a une erreur, revenez directement
		if e != nil {
			return al, e
		}

		for i, m := range c {
			point_map[m] = append(point_map[m], i)
		}
		for key := range point_map {
			keys = append(keys, key)
		}
		sort.Ints(keys)
		for i := len(keys) - 1; i >= 0; i-- {
			if len(point_map[keys[i]]) == 1 {
				al = append(al, point_map[keys[i]][0])
			} else if len(point_map[keys[i]]) > 1 {
				len := len(point_map[keys[i]])
				for j := 0; j < len; j++ {
					alt_tie, e := tiebreak(point_map[keys[i]])
					if e != nil {
						return al, e
					}
					al = append(al, alt_tie)
					indice := rank(alt_tie, point_map[keys[i]])
					point_map[i] = append(point_map[keys[i]][:indice], point_map[keys[i]][indice+1:]...)
				}
			}
		}
		return al, nil

	}
}

func SCFFactory(scf func(p Profile, alts []Alternative) ([]Alternative, error), tiebreak func([]Alternative) (Alternative, error)) func(Profile, []Alternative) (Alternative, error) {
	return func(p Profile, alts []Alternative) (Alternative, error) {
		best, e := scf(p, alts)
		if e != nil {
			return -1, e
		}
		best_tie, er := tiebreak(best)
		if er != nil {
			return -1, er
		}
		return best_tie, nil
	}
}

func CopelandSWF(p Profile, alts []Alternative) (Count, error) {
	//Le nombre total d'alternatives
	numAlts := len(p[0])

	// Initialiser une carte pour stocker le score de Copeland de chaque alternative
	copelandScores := make(map[Alternative]int)

	// Calculer le score de Copeland
	for i := 0; i < numAlts-1; i++ {
		a := p[0][i]
		for j := i + 1; j < numAlts; j++ {
			// Comparez chaque paire d'alternatives
			b := p[0][j]
			prefA := 0 // Nombre d'électeurs préférant l'alternative i
			prefB := 0 // Nombre d'électeurs préférant l'alternative j
			for _, pref := range p {
				if isPref(a, b, pref) {
					prefA++
				} else if isPref(b, a, pref) {
					prefB++
				}
			}
			if prefA > prefB {
				copelandScores[a]++
			} else if prefA == prefB {
				copelandScores[a] += 0
			} else {
				copelandScores[a]--
			}
			if prefB > prefA {
				copelandScores[b]++
			} else if prefB == prefA {
				copelandScores[b] += 0
			} else {
				copelandScores[b]--
			}
		}
	}

	return copelandScores, nil
}

func CopelandSCF(p Profile, alts []Alternative) (bestAlts []Alternative, err error) {
	copelandScores, err := CopelandSWF(p, alts)
	if err != nil {
		return nil, err
	}

	return maxCount(copelandScores), nil
}

// func STV_SWF(p Profile, alts []Alternative) (Count, error) {
func STV_SWF(p Profile, alts []Alternative) (Count, error) {
	//err := checkProfileAlternative(p, alts)
	f := p
	count := make(map[Alternative]int)
	calcul := make(map[Alternative]int)
	// if err != nil {
	// 	return count, err
	// }
	//Un total de len(alts)-1 tour
	n := len(f[0]) - 1
	for i := 0; i < n; i++ {
		{
			//Votez d'abord, calculez les votes
			for g := 0; g < len(f[0]); g++ {
				calcul[f[0][g]] = 0
			}
		}
		for j := 0; j < len(f); j++ {
			calcul[f[j][0]]++
		}
		//Trouvez la personne avec le moins de votes
		les_mauvais := minCount(calcul)
		mauvais, _ := TieBreak_SCF(les_mauvais)
		//La personne éliminée en premier obtient le score le plus bas
		count[mauvais] = i

		//Supprimez la personne avec le moins de votes de p et remettez le score dans le calcul à 0
		for j := 0; j < len(f); j++ {
			indice := rank(mauvais, f[j])
			f[j] = append(f[j][:indice], f[j][indice+1:]...)
			//calcul[p[j][0]] = 0
		}
		//Supprimez la personne avec le moins de votes du calcul
		calcul = make(map[Alternative]int)

		//delete(calcul, mauvais)

	}
	lastAlt := f[0][0]
	count[lastAlt] = n
	return count, nil
}

// func STV_SCF(p Profile, alts []Alternative) (bestAlts []Alternative, err error) {
func STV_SCF(p Profile, alts []Alternative) (bestAlts []Alternative, err error) {
	c, e := STV_SWF(p, alts)
	if e != nil {
		return bestAlts, e
	}
	bestAlts = maxCount(c)
	return bestAlts, e
}
