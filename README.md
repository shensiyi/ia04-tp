# IA04-TP
# Auteurs: SHEN Siyi, HUANG Yeming

# Installer le paquet:
go install gitlab.utc.fr/shensiyi/ia04-tp  
Sinon visiter le site https://gitlab.utc.fr/shensiyi/ia04-tp

# 7 Packages:
1.`main`: Execution du programme  
2.`ballotagent`: Le structure Ballotagent  
3.`voteragent`:Le structure de votant  
4.`comsoc`: Les différentes méthodes pour voter  
5.`restclientagent`: Les agents de clients  
6.`restserveragent`: L'agent de serveur  
7.`typejson`: Toutes les structures liée avec json  

# Organisme de fonction:
Ce projet réalise les simulations de vote et les intéractions entre les agents de clients et l'agent de serveur. Il possède trois fonctions principales avec trois urls:  
1.`/new_ballot`: créer un nouveau ballot de vote  
2.`/vote`: créer les votants dans le ballot et faire la vote  
3.`/result`: montrer le résultat de ballot de vote  

Nous avons crée des agents de client dans le package `restclientagent` pour chaque fonction de faire les requêtes et gérer la réponse de serveur. Et nous avons crée un agent de serveur dans le package `restserveragent` avec les méthodes de traiter chaque requête de chaque client.   

Il y a des règles entre trois fonctions:  
1.Il faut tout d'abord créer un nouveau ballot pour commencer à voter.  
2.Un votant peut voter pour un seul ballot et seul les votants dans le ballot peut voter pour ce ballot.  
3.Un ballot ne peut choisir que les méthodes dans le package `comsoc` pour voter.  
4.Un votant dans le ballot peut voter un seul fois et avant le deadline.  
5.On ne peut voir que le résultat après le deadline.  

# Méthode de tester:
1.Lancer lauch_all_rest_agents.go, les agents de clients et serveur vont tous lancer et on peut le voir comme un exemple de l'intéraction entre client et serveur.  
2.Lancer lauch-rsagt.go et le tester en utilisant plugin chrome.  