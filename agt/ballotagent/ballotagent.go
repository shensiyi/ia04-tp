package ballotagent

//Structure des données de ""ballot"", utilisée dans l'API new_ballot
import (
	"errors"
	"strconv"

	comsoc "gitlab.utc.fr/shensiyi/ia04-tp/comsoc"
)

// Type de Ballot
type Ballotagent struct {
	ID        string
	Rule      string
	Deadline  string
	Voter_ids []string
	Alts      int
	Tie_break []comsoc.Alternative
}

var counter int

type BallotMap map[string]Ballotagent

// Utilisé pour générer un nouveau ballot_id sans duplication
func GenerateBallotId() string {
	counter++
	str1 := "ballot"
	str2 := strconv.Itoa(counter)
	ballot := str1 + str2
	return ballot
}

func NewBallotagent(rule string, deadline string, voter_ids []string, alts int, tie_break []comsoc.Alternative) *Ballotagent {
	id := GenerateBallotId()
	return &Ballotagent{id, rule, deadline, voter_ids, alts, tie_break}
}

// Rechercher si l'id de ce votant existe dans le ballot
func (ba *Ballotagent) Find_Votant_in_Ballot(votant_id string) bool {
	ids := ba.Voter_ids
	for i := 0; i < len(ids); i++ {
		if ids[i] == votant_id {
			return true
		}
	}
	return false
}

// Rechercher si ce ballot existe or non
func (bm *BallotMap) Find_Ballot(ballot_id string) (*Ballotagent, error) {
	blt, found := (*bm)[ballot_id]
	if !found {
		return nil, errors.New("the ballot not exist")
	}
	return &blt, nil
}
