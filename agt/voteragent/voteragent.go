package voteragent

//Structure de données de ""Votant""", utilisée pour le vote API
import (
	comsoc "gitlab.utc.fr/shensiyi/ia04-tp/comsoc"
)

// Type de votant
type Agent struct {
	ID    string
	Prefs []comsoc.Alternative
}

// Utilisez l'id de chaque votant pour créer un lien, c'est-à-dire utilisez la map pour trouver le votant correspondant via l'id
type Votermap map[string]Agent

// Générer un nouveau vontant
func NewAgent(id string, prefs []comsoc.Alternative) *Agent {
	return &Agent{id, prefs}
}
