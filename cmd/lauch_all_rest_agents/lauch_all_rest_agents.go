package main

//Ce package est un exemple d'exécution simultanée du serveur et du client et doit être implémenté :
//1. Générer une nouvelle ballot
//2. Générer de nouveaux votants
//3. Afficher les résultats du vote
import (
	"fmt"
	"log"
	"sync"

	ballot "gitlab.utc.fr/shensiyi/ia04-tp/agt/ballotagent"
	votant "gitlab.utc.fr/shensiyi/ia04-tp/agt/voteragent"
	comsoc "gitlab.utc.fr/shensiyi/ia04-tp/comsoc"
	restclient "gitlab.utc.fr/shensiyi/ia04-tp/restclientagent"
	restserver "gitlab.utc.fr/shensiyi/ia04-tp/restserveragent"
)

var (
	mu sync.Mutex     // créer un mutex
	wg sync.WaitGroup // créer un groupe d'attente
)

func f1() {
	defer wg.Done()
	const url1 = ":8080"
	mu.Lock() // verrouiller le mutex
	servAgt := restserver.NewRestServerAgent(url1)
	log.Println("démarrage du serveur...")
	go servAgt.Start()
	mu.Unlock() // déverrouiller le mutex
	//wg.Done()   // indiquer que la tâche est terminée
	wg.Wait()

}

func startBallot(blt *ballot.Ballotagent) {
	defer wg.Done()
	mu.Lock() // verrouiller le mutex
	log.Println("démarrage des clients...")
	//blt := ballot.NewBallotagent("majarity", "2023-10-31 14:30:00", []string{"votan1", "votant2"}, 10, []comsoc.Alternative{1, 2, 3, 4, 5, 6, 7, 8, 9, 10})
	bltclient := restclient.NewRestClientAgentBallot("http://localhost:8080", *blt)
	go bltclient.StartBallot()
	mu.Unlock() // déverrouiller le mutex
}

func startVote(blt *ballot.Ballotagent) {
	defer wg.Done()
	mu.Lock() // verrouiller le mutex
	//blt := ballot.NewBallotagent("majarity", "2023-10-31 14:30:00", []string{"votan1", "votant2"}, 10, []comsoc.Alternative{1, 2, 3, 4, 5, 6, 7, 8, 9, 10})
	vot := votant.NewAgent("vatant1", []comsoc.Alternative{1, 3, 2, 10, 4, 5, 8, 7, 6, 9})
	votclient := restclient.NewRestClientAgentVote("http://localhost:8080", blt.ID, nil, *vot)
	go votclient.StartVote()
	mu.Unlock() // déverrouiller le mutex

}

func startResult(blt *ballot.Ballotagent) {
	defer wg.Done()
	mu.Lock() // verrouiller le mutex
	//blt := ballot.NewBallotagent("majarity", "2023-10-31 14:30:00", []string{"votan1", "votant2"}, 10, []comsoc.Alternative{1, 2, 3, 4, 5, 6, 7, 8, 9, 10})
	result := restclient.NewRestClientAgentResult("http://localhost:8080", blt.ID)
	go result.StartResult()
	mu.Unlock() // déverrouiller le mutex

}

func main() {
	wg.Add(4)
	blt := ballot.NewBallotagent("majarity", "2023-10-31 14:30:00", []string{"votan1", "votant2"}, 10, []comsoc.Alternative{1, 2, 3, 4, 5, 6, 7, 8, 9, 10})
	go f1()
	wg.Wait()
	go startBallot(blt)
	wg.Wait()
	go startVote(blt)
	wg.Wait()
	go startResult(blt)
	wg.Wait()
	fmt.Scanln()
}
