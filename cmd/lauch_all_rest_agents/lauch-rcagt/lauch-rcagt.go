package main

//Exécuter le client
import (
	"fmt"

	ballot "gitlab.utc.fr/shensiyi/ia04-tp/agt/ballotagent"
	votant "gitlab.utc.fr/shensiyi/ia04-tp/agt/voteragent"
	comsoc "gitlab.utc.fr/shensiyi/ia04-tp/comsoc"
	client "gitlab.utc.fr/shensiyi/ia04-tp/restclientagent"
)

func main() {
	blt := ballot.NewBallotagent("majarity", "2023-10-31 14:30:00", []string{"votan1", "votant2"}, 10, []comsoc.Alternative{1, 2, 3, 4, 5, 6, 7, 8, 9, 10})
	bltclient := client.NewRestClientAgentBallot("http://localhost:8080", *blt)
	bltclient.StartBallot()
	vot := votant.NewAgent("vatant1", []comsoc.Alternative{1, 3, 2, 10, 4, 5, 8, 7, 6, 9})
	votclient := client.NewRestClientAgentVote("http://localhost:8080", blt.ID, nil, *vot)
	votclient.StartVote()
	result := client.NewRestClientAgentResult("http://localhost:8080", blt.ID)
	result.StartResult()
	fmt.Scanln()
}
