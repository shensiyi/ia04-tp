package main

//Exécutez le serveur
import (
	"fmt"

	restserver "gitlab.utc.fr/shensiyi/ia04-tp/restserveragent"
)

func main() {
	server := restserver.NewRestServerAgent(":8080")
	server.Start()
	fmt.Scanln()
}
