package typejson

import (
	comsoc "gitlab.utc.fr/shensiyi/ia04-tp/comsoc"
)

type Request_ballot struct {
	Rule      string               `json:"rule"`
	Deadline  string               `json:"deadline"`
	Voter_ids []string             `json:"voter_ids"`
	Alts      int                  `json:"#alts"`
	Tie_break []comsoc.Alternative `json:"tie_break"`
}
type Response_ballot struct {
	Ballot_id string `json:"ballot_id"`
}
type Request_vote struct {
	Agent_id  string               `json:"agent_id"`
	Ballot_id string               `json:"ballot_id"`
	Prefs     []comsoc.Alternative `json:"preference"`
	Options   []int                `json:"options"`
}

type Request_result struct {
	Ballot_id string `json:"ballot_id"`
}

type Response_result struct {
	Winner  int                  `json:"winner"`
	Ranking []comsoc.Alternative `json:"rank"`
}
