package restclientagent

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	ballot "gitlab.utc.fr/shensiyi/ia04-tp/agt/ballotagent"
	votant "gitlab.utc.fr/shensiyi/ia04-tp/agt/voteragent"
	comsoc "gitlab.utc.fr/shensiyi/ia04-tp/comsoc"
	rad "gitlab.utc.fr/shensiyi/ia04-tp/type"
)

type RestClientAgentBallot struct {
	url    string
	ballot ballot.Ballotagent
}

type RestClientAgentVote struct {
	url       string
	ballot_id string
	options   []int
	votant    votant.Agent
}

type RestClientAgentResult struct {
	url       string
	ballot_id string
}

func NewRestClientAgentBallot(url string, ballot ballot.Ballotagent) *RestClientAgentBallot {
	return &RestClientAgentBallot{url, ballot}
}

func NewRestClientAgentVote(url string, ballot_id string, options []int, votant votant.Agent) *RestClientAgentVote {
	return &RestClientAgentVote{url, ballot_id, options, votant}
}

func NewRestClientAgentResult(url string, ballot_id string) *RestClientAgentResult {
	return &RestClientAgentResult{url, ballot_id}
}

// Traitez la réponse et décodez l'identifiant de vote de type JSON
func (rcab *RestClientAgentBallot) treatBallotResponse(r *http.Response) string {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)

	var resp rad.Response_ballot
	json.Unmarshal(buf.Bytes(), &resp)
	return resp.Ballot_id
}

func (rcar *RestClientAgentResult) treatResultResponse(r *http.Response) (int, []comsoc.Alternative) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)

	var resp rad.Response_result
	json.Unmarshal(buf.Bytes(), &resp)
	return resp.Winner, resp.Ranking
}

// Faire une requête de type POST à ​​Ballot
func (rcab *RestClientAgentBallot) doBallotRequest() (res string, err error) {
	req := rad.Request_ballot{
		Rule:      rcab.ballot.Rule,
		Deadline:  rcab.ballot.Deadline,
		Voter_ids: rcab.ballot.Voter_ids,
		Alts:      rcab.ballot.Alts,
		Tie_break: rcab.ballot.Tie_break,
	}
	url := rcab.url + "/new_ballot"
	data, _ := json.Marshal(req)
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))

	if err != nil {
		return
	}
	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		return
	}
	res = rcab.treatBallotResponse(resp)
	return
}

// Faire une demande POST pour voter
func (rcav *RestClientAgentVote) doVoteRequest() (err error) {
	req := rad.Request_vote{
		Agent_id:  rcav.votant.ID,
		Ballot_id: rcav.ballot_id,
		Prefs:     rcav.votant.Prefs,
		Options:   rcav.options,
	}
	url := rcav.url + "/vote"
	data, _ := json.Marshal(req)
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))

	if err != nil {
		return
	}
	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		return
	}
	return
}

// Faire une requête POST pour le résultat
func (rcar *RestClientAgentResult) doResultRequest() (res1 int, res2 []comsoc.Alternative, err error) {
	req := rad.Request_result{
		Ballot_id: rcar.ballot_id,
	}
	url := rcar.url + "/result"
	data, _ := json.Marshal(req)
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))

	if err != nil {
		return
	}
	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		return
	}
	res1, res2 = rcar.treatResultResponse(resp)
	return
}

func (rcab *RestClientAgentBallot) StartBallot() {
	log.Printf("démarrage Ballot")
	res, err := rcab.doBallotRequest()

	if err != nil {
		log.Fatal(rcab.ballot.ID, "error:", err.Error())
	} else {
		log.Println("get the message:", rcab.ballot.Rule, "\n", rcab.ballot.Deadline, "\n", rcab.ballot.Voter_ids, "\n", rcab.ballot.Alts, "\n", rcab.ballot.Tie_break)
		log.Println("reslut:", res)
	}
}

func (rcav *RestClientAgentVote) StartVote() {
	log.Printf("démarrage vote")
	err := rcav.doVoteRequest()

	if err != nil {
		log.Fatal(rcav.votant.ID, "error:", err.Error())
	} else {
		log.Println("get the message:", rcav.votant.ID, "\n", rcav.ballot_id, "\n", rcav.votant.Prefs, "\n", rcav.options)
		log.Println("vote sucessfully")
	}
}

func (rcar *RestClientAgentResult) StartResult() {
	log.Printf("the result")
	res1, res2, err := rcar.doResultRequest()

	if err != nil {
		log.Fatal(rcar.ballot_id, "error:", err.Error())
	} else {
		log.Println("get the message:", rcar.ballot_id)
		log.Println("winner:", res1, "ranking:", res2)
	}
}
